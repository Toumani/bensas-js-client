import React, { Component } from 'react';
import { Button, Glyphicon } from 'react-bootstrap';

import './style/stylesheets/NavButton.css';

class NavButton extends Component {
	render() {
		return (
			<Button bsSize="large" className="nav-button" onClick={this.props.onClick}>
				<Glyphicon glyph={ this.props.glyph } />
				{ this.props.text }
			</Button>
        );
    }
}

export default NavButton;
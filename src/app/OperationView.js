import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class Operation extends Component {
	render() {
		let content = Array();
		for (let i = 0; i < 5; i++) {
			content.push(
					<tr>
						<td>1</td>
						<td>002451125177859</td>
						<td>002499857458362</td>
						<td>$ { 24.00 * i }</td>
					</tr>
			);
		}
		return (
			<Table striped bordered condensed hover>
				<thead>
					<tr>
						<th>#</th>
						<th>Compte émetteur</th>
						<th>Compte destinataire</th>
						<th>Montant</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>002451125177859</td>
						<td>002499857458362</td>
						<td>$24.00</td>
					</tr>
					{ content }
				</tbody>
			</Table>
		)
	}
}

export default Operation;
import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-bootstrap';

import VBox from './VBox';
import NavButton from './NavButton';

import './style/stylesheets/Wrapper.css';


class Wrapper extends Component {
	render() {
		return (
			<div id="Wrapper">
				<div id="side-bar">
					<div id="user-infos">
						<h4>Toumani SIDIBE</h4>
						<p>Jnane Chekkouri Imm. Salam bloc 4</p>
						<p>+212 6 99 785 777</p>
					</div>
					<VBox>
						<NavButton text="Comptes" glyph="console" onClick={this.props.showAccount} />
						<NavButton text="Opérations" glyph="sort" onClick={this.props.showOperation}/>
						<NavButton text="Recharge" glyph="earphone" />
					</VBox>
					<div id="exit-container">
						<VBox>
							<NavButton text="Déconnexion" glyph="remove" />
						</VBox>
					</div>
				</div>
				<div id="content">
					<div id="header" className="show-grid">
						<h1>
							{ this.props.children.props.title }
						</h1>
					</div>
					<div id="wrapped">
						{ this.props.children }
					</div>
				</div>
			</div>

		);
	}
}

export default Wrapper;
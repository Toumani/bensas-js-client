import React, { Component } from 'react';

import './style/stylesheets/VBox.css';

class VBox extends Component {
    render() {
        return (
            <div className="vbox">
				{ this.props.children }
			</div>
        );
    }
}

export default VBox;
import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import './style/stylesheets/AccountView.css';

class AccountView extends Component {
    render() {
		 const content = this.props.children.map(
			child => (
				<Col xs={12} md={4}>{ child }</Col>
			)
		)
        return (
			<Grid className="account-view">
				<Row>
					{ content }
				</Row>
			</Grid>
        );
    }
}

export default AccountView;
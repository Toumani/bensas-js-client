import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import logo from '../logo.svg';

import Wrapper from './Wrapper';
import AccountView from './AccountView';
import Account from './Account';
import OperationView from './OperationView';

import './style/stylesheets/App.css';
import Operation from './OperationView';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			view: 'operation'
		};
		/* this.setState({
			view: 'operation'
		}) */
	}

	showAccount = (e) => {
		this.setState({
			view: 'account'
		});
		console.log('Switching view to Account');
	}

	showOperation = (e) => {
		this.setState({
			view: 'operation'
		});
		console.log('Switching view to Operation');
	}

	showPhone = (e) => {
		this.setState({
			view: 'phone'
		});
		console.log('Switching view to Phone');
	}

	render() {
		let content;
		switch (this.state.view) {
			case 'account':
				content = (
					<AccountView title="Comptes">
						<Account />
						<Account />
						<Account />
						<Account />
					</AccountView>
				);
				break;
			case 'operation':
				content = (
					<OperationView title="Opérations" />
				)
				break;
			case 'phone':
				break;
		}
		return (
			<div className="App">
				{/* <header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<p>
					Edit <code>src/App.js</code> and save to reload.
					</p>
					<a
					className="App-link"
					href="https://reactjs.org"
					target="_blank"
					rel="noopener noreferrer"
					>
					Learn React
					</a>
				</header> */}

				<Wrapper showAccount={ this.showAccount } showOperation={ this.showOperation }>
					{ content }
				</Wrapper>
			</div>
		);
	}
}

export default App;

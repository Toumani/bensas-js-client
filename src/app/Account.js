import React, { Component } from 'react';
import { Glyphicon } from 'react-bootstrap';

import './style/stylesheets/Account.css';

class Account extends Component {
    render() {
		return (
			<div className="account">
				<div className="badge">
					<Glyphicon glyph="console" />
				</div>
				<div className="content">
					<p className="small">RIB: 4032654558</p>
					<p>Agence de Markala</p>
				</div>
			</div>
		);
    }
}

export default Account;